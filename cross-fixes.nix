self: super: {
  openssh = super.openssh.override {
    withKerberos = false;
  };
}
