{ lib, ... }:

let
  inherit (lib) flip mkMerge mapAttrsToList;

  keys = {
    barbara = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDltmS7+BEHOcLUS9O/NeoC2oic+R56c9GTJ2ixuBp56 root@barbara";
    kevin = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPP72c7kAb9w/q+mxjQ1lPIU6IuN86hEiNH8KPA/aHL3 root@kevin";
    bonbori = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHPYxcBuQ/DGH2lJd4EQqqq4oeGj/cCeTwJkKhCn7yLz root@bonbori";
    illumination = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDWHMsLgnIfpX6LLdITDxrnH5puKZZqn8JGcy0K3Y207 root@illumination.cons.org.nz";
  };

in

mkMerge (flip mapAttrsToList keys (name: key: {
  users.users."${name}" = {
    isNormalUser = true;
    openssh.authorizedKeys.keys = [ key ];
  };

  nix.trustedUsers = [ name ];
}))
