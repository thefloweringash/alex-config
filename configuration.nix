{ config, lib, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./lorne-nix
    ./remote-builder.nix
    ./build-vm.nix
    ./thefloweringash-armv7.cachix.org.nix
  ];

  nixpkgs.overlays = [ (import ./lx2k-nix/overlay.nix) ];

  boot.loader.grub.enable = false;
  boot.loader.generic-extlinux-compatible.enable = true;
  boot.loader.generic-extlinux-compatible.configurationLimit = 5;

  # Fails to include modules that aren't built by the vendor kernel config.
  boot.initrd.includeDefaultModules = false;

  boot.kernelPackages = pkgs.linuxPackages_lx2k;
  boot.kernelParams = [
    "panic=60"
    "console=ttyAMA0,115200"
    "earlycon=pl011,mmio32,0x21c0000"
    "pci=pcie_bus_perf"
    "arm-smmu.disable_bypass=0" # TODO: remove once firmware supports it
  ];
  boot.kernelPatches = [{
    name = "fix-cp210x";
    patch = ./fix-cp201x.patch;
  }];

  boot.cleanTmpDir = true;

  networking.hostName = "alex";
  networking.domain = "cons.org.nz";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.eth0.useDHCP = true;

  environment.systemPackages = with pkgs; [
    ag
    ethtool
    file
    fzf
    gitFull
    gitAndTools.diff-so-fancy
    gitAndTools.delta
    gotop
    jq
    openssl
    python
    powerline-go
    stow
    tcpdump
    tig
    tmux
    tree
    tshark
    units
    vim
    lm_sensors
    i2c-tools
    multipath-tools
    tree
    morph
    s-tui
    nix-top
  ];

  programs.mosh.enable = true;
  programs.zsh.enable = true;

  users.users.lorne = {
    uid = 1000;
    isNormalUser = true;
    extraGroups = [ "wheel" "docker" "dialout" ];
    shell = pkgs.zsh;
  };

  lorne-nix.emacs.enable = true;
  lorne-nix.monitoring.enable = true;

  time.timeZone = "Asia/Tokyo";

  services.openssh.enable = true;
  services.openssh.passwordAuthentication = false;
  services.openssh.challengeResponseAuthentication = false;
  services.openssh.forwardX11 = true;

  services.fstrim.enable = true;

  services.prometheus.exporters.node.openFirewall = true;

  systemd.enableUnifiedCgroupHierarchy = false;
  virtualisation.containers.enable = true;
  virtualisation.docker.enable = true;

  services.buildVMs =
    let common = { pkgs, ... }: {
      imports = [
        ./remote-builder.nix
        ./thefloweringash-armv7.cachix.org.nix
      ];

      users.mutableUsers = false;

      users.users.alex = {
        isNormalUser = true;
        openssh.authorizedKeys.keys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEdTJVdEsJbY7XuVP0yd5zyaZl2BXE2G6SDGtvhSyvGN root@alex"
        ];
      };

      users.users.root = {
        openssh.authorizedKeys.keys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEdTJVdEsJbY7XuVP0yd5zyaZl2BXE2G6SDGtvhSyvGN root@alex"
        ];
      };

      nix.trustedUsers = [ "alex" ];

      environment.systemPackages = with pkgs; [ nix-top ];

      nixpkgs.overlays = [ (import ./cross-fixes.nix) ];

      boot.kernel.sysctl."kernel.sysrq" = 1;

      # slim builders down these a bunch
      programs.command-not-found.enable = false;
      security.polkit.enable = false;
      services.udisks2.enable = false;
      documentation.enable = false;
      environment.noXlibs = true;
    };
    in
    {
      # cross built 32 bit arm (armv5tel,armv6l,armv7l}-linux builder
      alex7 = {
        system = "aarch64-linux";
        cpu = "host,aarch64=off";
        smp = 16;
        mem = "8g";
        consoleListenPort = 2100;
        sshListenPort = 2200;
        watchdog.enable = true;
        config = { pkgs, ... }: {
          imports = [ common ];
          nixpkgs.crossSystem = { system = "armv7l-linux"; };
          nix.extraOptions = ''
            extra-platforms = armv5tel-linux armv6l-linux
          '';
          # Using latest kernel with a serial port for debugging
          ## 5.14.x seems to hang almost immediately, let's try default
          # boot.kernelPackages = pkgs.linuxPackages;
        };
      };
    };


  system.autoUpgrade.enable = true;
  system.autoUpgrade.allowReboot = true;

  nix.distributedBuilds = true;
  nix.buildMachines = [
    {
      hostName = "alex7";
      sshUser = "alex";
      sshKey = "/root/.ssh/id_ed25519";
      systems = [ "armv5tel-linux" "armv6l-linux" "armv7l-linux" ];
      maxJobs = 8;
      supportedFeatures = [ "big-parallel" ];
    }
  ];

  nix.extraOptions = ''
    secret-key-files = /etc/nix/signing.key
  '';

  programs.ssh.extraConfig = ''
    Host alex7
        HostName localhost
        Port 2200
  '';

  nix.sshServe = {
    enable = true;
    keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG1ekNCU3h0vJc6mtNzZyGU9oUVpdd9lbEdR5mrXbfJQ root@bpim3"
    ];
  };

  networking.firewall.allowedTCPPorts = [ 2200 ];

  # http://github.com/NixOS/nixpkgs/pull/73533
  networking.firewall.checkReversePath = true;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?

  boot.kernel.sysctl."kernel.sysrq" = 1;

  nix.trustedUsers = [ "lorne" ];

}

